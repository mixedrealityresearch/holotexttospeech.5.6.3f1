﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var soundManager = GameObject.Find("Audio Manager");
        TextToSpeechManager textToSpeech = soundManager.GetComponent<TextToSpeechManager>();
        textToSpeech.Voice = TextToSpeechVoice.Mark;
        textToSpeech.SpeakText("안녕하세요! Welcome to the Holographic App! You can use Gaze, Gesture and Voice Command to interact with it!");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
